/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sgl.view.fornecedor;
import br.com.sgl.DAO.FornecedorDAO;
import br.com.sgl.projeto.Fornecedor;


import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author emersom
 */
public class ConsultaFornecedores extends javax.swing.JFrame {

    /**
     * Creates new form ConsultaFuncionario
     */
    public ConsultaFornecedores() {
        initComponents();
        this.readJtable();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        butSelecionar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTFornecedor = new javax.swing.JTable();
        butVisuaEdit = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SGL Light - Consultar Funcionário");

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/sgl/imagens/icon_sgl.png"))); // NOI18N

        jLabel2.setText("ID");

        txtID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIDActionPerformed(evt);
            }
        });

        butSelecionar.setText("Selecionar ID");
        butSelecionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butSelecionarActionPerformed(evt);
            }
        });

        jTFornecedor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "            ID", "                                                        Nome"
            }
        ));
        jScrollPane2.setViewportView(jTFornecedor);
        if (jTFornecedor.getColumnModel().getColumnCount() > 0) {
            jTFornecedor.getColumnModel().getColumn(0).setMinWidth(100);
            jTFornecedor.getColumnModel().getColumn(0).setPreferredWidth(10);
            jTFornecedor.getColumnModel().getColumn(0).setMaxWidth(10);
        }

        butVisuaEdit.setText("Vizualizar/Editar");
        butVisuaEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butVisuaEditActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(230, 230, 230))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(butSelecionar)
                        .addGap(229, 229, 229))
                    .addComponent(butVisuaEdit)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 476, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(33, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(63, 63, 63)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(butSelecionar)
                    .addComponent(jLabel2))
                .addGap(30, 30, 30)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(63, 63, 63)
                .addComponent(butVisuaEdit)
                .addContainerGap(115, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txtIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIDActionPerformed

    private void butVisuaEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butVisuaEditActionPerformed
        try {
            Fornecedor f = null;
            FornecedorDAO fd = new FornecedorDAO();
            f = fd.buscaFornecedor(Integer.parseInt(this.validaCaracter(jTFornecedor.getValueAt(jTFornecedor.getSelectedRow(), 0).toString())));
            if(f != null){
                TelaEditarFornecedor ef = new TelaEditarFornecedor(f);
                ef.setVisible(true);
                dispose();
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "Selecione um Fornecedor pelo ID.");
        }
                
    }//GEN-LAST:event_butVisuaEditActionPerformed

    private void butSelecionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butSelecionarActionPerformed

        this.procuraFunc(this.validaCaracter(txtID.getText()));
    }//GEN-LAST:event_butSelecionarActionPerformed

    
    
    public void readJtable(){
        FornecedorDAO ce = new FornecedorDAO();
        DefaultTableModel modelo = (DefaultTableModel) jTFornecedor.getModel();
        modelo.setNumRows(0);
        
        
            for(int i =0; i < ce.listaFornecedores().size(); i++){
                Fornecedor p = ce.listaFornecedores().get(i);
                modelo.addRow(new Object[]{
                    p.getIdFor(),
                    p.getNome()
                });
            }
           
    }
   
    public void procuraFunc(String id){
         DefaultTableModel dtm = (DefaultTableModel) jTFornecedor.getModel();
         int linhas =  jTFornecedor.getRowCount();
         int indiceLocalizado = -1;
         try {
            for (int i = 0; i < linhas; i++) {
                String d = (String) dtm.getValueAt(i,0).toString();
               if (d.equals(id)){
                   indiceLocalizado = i;
                   break;
               } else {
               }
            }
            
            if(indiceLocalizado < 0){
                JOptionPane.showMessageDialog(null,"Fornecedor não encontrado.\nDigite o ID novamente.");
            }else{
                    jTFornecedor.setRowSelectionInterval(indiceLocalizado, indiceLocalizado);
            }
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Fornecedor não Encontrado.");
        }    
        
    }   
    
    public String validaCaracter(String dado){
        return dado.replaceAll("[^0-9]+", "");
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConsultaFornecedores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConsultaFornecedores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConsultaFornecedores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConsultaFornecedores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ConsultaFornecedores().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton butSelecionar;
    private javax.swing.JButton butVisuaEdit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTFornecedor;
    private javax.swing.JTextField txtID;
    // End of variables declaration//GEN-END:variables
}

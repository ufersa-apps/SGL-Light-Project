package br.com.sgl.databse;
import java.sql.*;
import javax.swing.JOptionPane;

public class ConnectionFactory {
    public static Connection getConection(String strURL,String strUser,String strPassword) throws ClassNotFoundException {
        String url = strURL;
        String user = strUser;
        String pass = strPassword;
        try{
            Class.forName("org.postgresql.Driver");
            Connection con = DriverManager.getConnection(url,user,pass);
            //JOptionPane.showMessageDialog(null, "Conexão com o banco de dados bem sucessida !");
            return con;
        }
        catch(SQLException error){
            JOptionPane.showMessageDialog(null, error);
            return null;
        }  
    }
}

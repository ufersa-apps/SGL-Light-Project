package br.com.sgl.projeto;



import java.time.LocalDate;

/**
 *
 * @author Gabriel
 */
public class Pessoa {
    public String nome;
    public String rua;
    public int numero;
    public String bairro;
    public String cidade;
    private LocalDate aniversario;
    private String cpf;

    public Pessoa(String nome, String rua, int numero, String bairro, String cidade, LocalDate aniversario, String cpf) {
        this.nome = nome;
        this.rua = rua;
        this.numero = numero;
        this.bairro = bairro;
        this.cidade = cidade;
        this.aniversario = aniversario;
        this.cpf = cpf;
    }
    
    
    public void editaPessoa(String nome, String bairro, String cidade,String rua ,LocalDate   aniversario, String cpf, int n){
        this.nome = nome;
        this.cpf = cpf;
        this.rua =  rua;
        this.aniversario = aniversario;
        this.bairro = bairro;
        this.cidade = cidade;
        this.numero = n;      
    }

    public String getNome() {
        return nome;
    }    

    public String getCidade() {
        return cidade;
    }
   

    public LocalDate getAniversario() {
        return aniversario;
    }

    public String getCpf() {
        return cpf;
    }

    public int getNumero() {
        return numero;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public void setAniversario(LocalDate aniversario) {
        this.aniversario = aniversario;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    
    
}

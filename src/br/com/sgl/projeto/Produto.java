package br.com.sgl.projeto;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
public class Produto {
    private int id;
    private int quantidade;
    private String nome;
    private float valor;
    private float valorCompra;
    private LocalDate validade;

    public Produto(String nome, String marca, float valor, float valorCompra,int quantidade) {
        this.nome = nome;
        this.valor = valor;
        this.valorCompra = valorCompra;
        this.quantidade = quantidade;
    }

    Produto() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getNome() {
        return nome;
    }

    public float getValor() {
        return valor;
    }

    public float getValorCompra() {
        return valorCompra;
    }

    public LocalDate getValidade() {
        return validade;
    }

    public void setValidade(LocalDate validade) {
        this.validade = validade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public void setValorCompra(float valorCompra) {
        this.valorCompra = valorCompra;
    }
   
    
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sgl.projeto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;

/**
 *
 * @author Gabriel Lopes
 */
public class  Compra {
    public int id_venda;
    public LocalDate data;
    public LocalTime hora;
    private float subTotal;

    public Compra() {
    }

    public Compra( LocalDate data, LocalTime hora, float subTotal) {
        this.id_venda = id_venda;
        this.data = data;
        this.hora = hora;
        this.subTotal = subTotal;
    }
    
    
    
    public void addCompra(){
        this.subTotal = 0;
    }

    public float getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(float subTotal) {
        this.subTotal = subTotal;
    }
    
    public void addSubtotal(float val){
        this.subTotal += val;
    }
    public void removeSubtotal(float val){
        this.subTotal-= val;
    }

    public int getId_venda() {
        return id_venda;
    }

    public void setId_venda(int id_venda) {
        this.id_venda = id_venda;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public LocalTime getHora() {
        return hora;
    }

    public void setHora(LocalTime hora) {
        this.hora = hora;
    }

 
    
}

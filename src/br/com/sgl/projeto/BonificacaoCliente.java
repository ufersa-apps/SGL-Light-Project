/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.sgl.projeto;


public class BonificacaoCliente {
    private float limite;
    private float desconto;
    private boolean ativo = false;

    public BonificacaoCliente() {
    
    }
    
    
    public void bonusAtivo(boolean ativo){
       this.ativo = ativo; 
    }
    public boolean getAtivo(){
        return this.ativo;
    }
    public void zeraLimite(){
        this.limite = 0;  
    }
    
    public void setDesconto_(float valor){
        this.desconto = valor; 
    }
    public void setDesconto(int valor){
        this.desconto = valor/(float)100; 
    }
    public void setLimite(float limite){
        this.limite = limite;
    }
    public float getlimite(){
        return this.limite;
    }
    
    public float  getDesconto(){
        return this.desconto; 
    }
    
}

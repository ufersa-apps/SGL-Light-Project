package br.com.sgl.projeto;
import br.com.sgl.DAO.LoginDAO;
import java.sql.SQLException;
/**
 *
 * @author Gabriel
 */
public class Login {
    private String nome;
    private String senha;
    
    public void cadastrarLogin(String n, String s) throws SQLException{
        LoginDAO ld = new LoginDAO();
        ld.editaLogin(n, s);
    }
    public void editaLogin(String s, String n){
        nome = n;
        senha = s;
    }
   
    public boolean validaLogin(Login l ){
       LoginDAO ld = new LoginDAO();
        return ld.validaLogin(l);
    }
    
    
    public String getNome() {
        return nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    
    
}

package br.com.sgl.projeto;

import java.util.Date;
import br.com.sgl.projeto.BonificaFuncionario;

/**
 *
 * @author Gabriel
 */

public class Funcionario extends Pessoa {
    private int idFunc;
    private float totalVendido = 0.0f; 
    private float salario;
    public String telefone;
     
    public Funcionario(String nome,float salario,String rua, int numero, String bairro, String cidade,  String cpf) {
        super(nome, rua, numero, bairro, cidade, null, cpf);
        this.setSalario(salario);
    }

    public int getIdFunc() {
        return idFunc;
    }

    public void setIdFunc(int idFunc) {
        this.idFunc = idFunc;
    }

    public float getSalario() {
        return salario;
    }
    
    public void setTotalVendido(float valorVendido){
        this.totalVendido += valorVendido;
    }
    
    public void zerarTotalVendido(){
        this.totalVendido = 0.0f;
    }
    
    public float getTotalVendido(){
        return this.totalVendido; 
    }
    
    public void zeraTvendas(){
        this.totalVendido = 0.0f;
    }
    
    public void setSalario(float salario) {
        this.salario = salario;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}

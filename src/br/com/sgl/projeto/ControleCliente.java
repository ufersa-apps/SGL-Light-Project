/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sgl.projeto;

import br.com.sgl.DAO.ClienteDAO;
import java.awt.List;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Gabriel Lopes
 */
public class ControleCliente {
   
    
    public void cadastraCCliente(Cliente cliente){
        ClienteDAO c = new ClienteDAO();
        c.cadastraCliente(cliente);
    }
    
    public Cliente buscaClienteR(int id){
        ClienteDAO cli = new ClienteDAO();
            Cliente c = null;
        if(id >0){            
            c = cli.buscaClienteID(id);
        }else{
            return null;
        }
        return c;
    }
    
   public ArrayList<Cliente> LisCliente(){
        ClienteDAO cli = new ClienteDAO();
        return (ArrayList<Cliente>) cli.listaCliente();
    }
   public Cliente buscaClienteR(String cl){
       ArrayList<Cliente> lisCli = this.LisCliente();
       Cliente aux = null;
       for(int i = 0;i < lisCli.size(); i++){
           aux = lisCli.get(i);
           if(aux.getNome().equals(i)){
               break;
           }
           else{
               aux = null;
           }     
    }
       return aux;
   }
   
   public void removeCliente(Cliente c){
       ClienteDAO cli = new ClienteDAO();
       cli.removeCliente(c);
   }
   
}

package br.com.sgl.projeto;
import br.com.sgl.DAO.ControleEstoqueDAO;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ControleEstoque {
    private Produto mercadoria = null;
    private int quantidade = 0;
    
    public void cadastrarMercadoria(Produto mercadoria){
       ControleEstoqueDAO cbd = new ControleEstoqueDAO();
        try {
            cbd.cadastraMercadoria(mercadoria, mercadoria.getQuantidade());
        } catch (SQLException ex) {
            Logger.getLogger(ControleEstoque.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void editaInfoProduto(Produto mercadoriaEdit,int nQuantidade) throws SQLException{
        ControleEstoqueDAO cbd = new ControleEstoqueDAO();
        cbd.editarInfoProduto(mercadoriaEdit, nQuantidade);
    }
    
    
    public List<Produto> listaProduto() throws SQLException{
        ControleEstoqueDAO cbd = new ControleEstoqueDAO();
        return cbd.listaProduto(); 
    } 
    
    public Produto buscaProduto(int id) throws SQLException{
       /* List<Produto> lp = this.listaProduto();
        Produto aux = lp.get(lp.size() - 1);
        int ult = aux.getId();
        Produto p =  null;
        if(id > 0 && id <= ult){    
            for(int i = 0; i < lp.size(); i++){
                p = lp.get(i);
                if(p.getId() == id){
                    break;
                }
            }
            return p;
        }else{
            JOptionPane.showMessageDialog(null, "ID de produto inexistente");
            return p;
        }*/
        Produto p =  null;
        ControleEstoqueDAO ced = new ControleEstoqueDAO();
        if(id > 0){
            p = ced.buscaProduto(id);
        }else{
            p = null;
        }
        return p;
    }
    
    public Produto buscaProduto(String descricao){
        Produto p = null;
        ControleEstoqueDAO  ced = new ControleEstoqueDAO();
        p = ced.buscaProduto(descricao);
        /*List<Produto> lp = this.listaProduto();
        Produto p = null;
        for(int i = 0;i < lp.size(); i++){
            p = lp.get(i);
            if(p.getNome().equals(descricao)){
                break;
            }else{
                p = null;
            }
        }*/
        return p;
    }
    
    public void removeProduto(Produto p, int quantidadeRemove){
        ControleEstoqueDAO ced = new ControleEstoqueDAO();
        ced.removeProduto(p, quantidadeRemove);
    }
    
}


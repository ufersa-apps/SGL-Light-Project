/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sgl.projeto;

/**
 *
 * @author belarsn
 */
    
public class BonificaFuncionario{
     
    private float meta;
    private float bonus; 
    private boolean ativo;

    public void setAtivo(boolean ativo){
        this.ativo = ativo;
    }
    
    public boolean getAtivo(){
        return this.ativo;
    }
    
    
    public void setMeta(float meta){
        this.meta = meta; 
    }
    
    public float getMetas(){
        return this.meta;
    }
    
    public void setBonus(float bonus){
        this.bonus = bonus/(float)100;
    }
    
    public float getBonus (){
        return this.bonus;
    }
}

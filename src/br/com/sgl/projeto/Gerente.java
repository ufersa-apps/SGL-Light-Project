package br.com.sgl.projeto;

import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author Gabriel
 */
public class Gerente extends Pessoa{
    public int iDadm;
    private Login log;

    public Gerente(int iDadm, Login log, String nome, String rua, int numero, String bairro, String cidade, LocalDate aniversario, String cpf) {
        super(nome, rua, numero, bairro, cidade, aniversario, cpf);
        this.iDadm = iDadm;
        this.log = log;
    }
    
    
    public void editaLoginGerente(Login log, String n, String s){
        log.editaLogin(s, n);        
    }
}
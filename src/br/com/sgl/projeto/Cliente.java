package br.com.sgl.projeto;

import java.time.LocalDate;

/**
 *
 * @author Gabriel
 * @author Belarmino
 */
/** Lembrar de Criar mo Metodo Para Bonificacao!!!*/
public class Cliente extends Pessoa{
    private int idCliente;
    private Conta contaCliente;

    private float totalComprado;   

    public String telefone;
    public String email;
    private int idConta;

    public Cliente(Conta contaCliente, String telefone, String email, String nome, String rua, int numero, String bairro, String cidade, String cpf) {
        super(nome, rua, numero, bairro, cidade, null, cpf);
        this.contaCliente = contaCliente;

        

        this.telefone = telefone;
        this.email = email;
    }

    public float getTotalComprado() {
        return totalComprado;
    }

    public void setTotalComprado(float totalComprado) {
        this.totalComprado = totalComprado;
    }
    
    

    public int getIdCliente() {
        return idCliente;
    }

    public Conta getContaCliente() {
        return contaCliente;
    }

    public String getNome() {
        return nome;
    }

    public String getCidade() {
        return cidade;
    }

    public int getNumero() {
        return numero;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public void setContaCliente(Conta contaCliente) {
        this.contaCliente = contaCliente;
    }

    public float getBonus() {
        return totalComprado;
    }

    public void setBonus(float bonus) {
        this.totalComprado = bonus;

    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdConta() {
        return idConta;
    }

    public void setIdConta(int idConta) {
        this.idConta = idConta;
    }
    
    
}

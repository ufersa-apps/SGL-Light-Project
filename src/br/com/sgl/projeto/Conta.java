package br.com.sgl.projeto;

import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author Gabriel
 */
public class Conta {
    private float crediario = 0;
    LocalDate data = null;
    private int id_deps;
    
    public Conta(float v){
        crediario = v;
        data = null;     
    }
    
    public float getCrediario() {
        return crediario;
    }

    public LocalDate getData() {
        return data;
    }

    public void setCrediario(float crediario) {
        this.crediario = crediario;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public int getId_deps() {
        return id_deps;
    }

    public void setId_deps(int id_deps) {
        this.id_deps = id_deps;
    }
    
    
}

    
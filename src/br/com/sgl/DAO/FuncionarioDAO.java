/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sgl.DAO;

import br.com.sgl.projeto.Funcionario;
import br.com.sgl.databse.ConnectionFactory;
import java.sql.*;
import javax.swing.JOptionPane;
import br.com.sgl.projeto.ControleEstoque;
import br.com.sgl.projeto.Produto;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jadson Bruno
 */

public class FuncionarioDAO {
    private Connection con;
    
    public FuncionarioDAO() {
        try {
            this.con =  ConnectionFactory.getConection("jdbc:postgresql://localhost:5432/sgl", "postgres", "brasfoot");
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null,"Erro na conexÃ£o com o Banco de Dados");
        }
    }
    
    /**Cadastra o FuncionÃ¡rio passado como parÃ¢metro no Banco de Dados*/
    public void cadastraFuncionario(Funcionario f){
        String sql = "insert into funcionario (nome,salario,cpf,cidade,numero,bairro,rua,telefone) values (?,?,?,?,?,?,?,?)";
        
        try {
                PreparedStatement pst = con.prepareStatement(sql);
                    
                pst.setString(1,f.getNome());
                pst.setFloat(2,f.getSalario());
                pst.setString(3,f.getCpf());
                pst.setString(4, f.getCidade());
                pst.setInt(5, f.getNumero());
                pst.setString(6,f.getBairro());
                pst.setString(7, f.getRua());
                pst.setString(8,f.getTelefone());

               pst.execute();
           } catch (SQLException error) {
                JOptionPane.showMessageDialog(null, error);
           }
    }
    /**Retorna o Funcionario com o ID passado ou  null caso nÃ£o encontre um Funcionario*/ 
    public Funcionario buscaFuncionario(int id){
        Funcionario b = null;
        String sql = "select *from funcionario where id_func = ?";
        
        
        try {
               PreparedStatement pst = con.prepareStatement(sql);
               pst.setInt(1, id);
               ResultSet rs = pst.executeQuery();
               while(rs.next()){
                    b = new Funcionario(rs.getString("nome"),rs.getFloat("salario"),rs.getString("rua"),rs.getInt("numero"),rs.getString("bairro"),rs.getString("cidade"),rs.getString("cpf"));
                    b.setIdFunc(rs.getInt("id_func"));
                    b.setTelefone(rs.getString("telefone"));
               }
                
           } catch (SQLException e) {
               JOptionPane.showMessageDialog(null, e);
           }
        
        return b;
    }
    /**Retorna um Funcionario que possua o mesmo nome passado como parÃ¢metro ou null, caso nÃ£o encontre nenhum com o mesmo nome no banco de dados. */
    public Funcionario buscaFuncionario(String nome){
        Funcionario b = null;
        String sql = "select *from funcionario where nome = ?";
        
        
        try {
               PreparedStatement pst = con.prepareStatement(sql);
               pst.setString(1,nome);
               ResultSet rs = pst.executeQuery();
               while(rs.next()){
                    b = new Funcionario(rs.getString("nome"),rs.getFloat("salario"),rs.getString("rua"),rs.getInt("numero"),rs.getString("bairro"),rs.getString("cidade"),rs.getString("cpf"));
                    b.setIdFunc(rs.getInt("id_func"));
                    b.setTelefone(rs.getString("telefone"));
               }
                
           } catch (SQLException e) {
               JOptionPane.showMessageDialog(null, e);
           }
        
        return b;
    }
    
     /**Lista todos os funcionÃ¡rios armazenados no Banco de Dados.Leia os comentÃ¡rios:*/
    /*MÃ©todo ainda nÃ£o testado. */
    
     public List<Funcionario> listaFuncionario() throws SQLException{
           String sql = "select*from funcionario";
           
           List<Funcionario> lfunc =  new ArrayList<>();
           try {
               PreparedStatement pst = con.prepareStatement(sql);
               ResultSet rs = pst.executeQuery();
               while(rs.next()){
                  Funcionario f = new Funcionario(rs.getString("nome"),rs.getFloat("salario"),rs.getString("rua"),rs.getInt("numero"),rs.getString("bairro"),rs.getString("cidade"),rs.getString("cpf"));
                  f.setIdFunc(rs.getInt("id_func"));
                  f.setTelefone(rs.getString("telefone"));
                  lfunc.add(f);
               }
               
           } catch (SQLException error) {
               JOptionPane.showMessageDialog(null, error);
           }
            return lfunc;
       }
     /**Edita o funcionÃ¡rio armazenado com o mesmo ID do Objeto FuncionÃ¡rio Passado como ParÃ¢metro com as informÃ§Ãµes deste.*/
     public void editaFuncionario(Funcionario f){
         String sql = "update funcionario set nome = ?,salario = ?,cpf = ?,cidade = ?,numero = ?,bairro = ?,rua = ?,telefone = ? where id_func = ?";
          try {
                PreparedStatement pst = con.prepareStatement(sql);
                    
                pst.setString(1,f.getNome());
                pst.setFloat(2,f.getSalario());
                pst.setString(3,f.getCpf());
                pst.setString(4, f.getCidade());
                pst.setInt(5, f.getNumero());
                pst.setString(6,f.getBairro());
                pst.setString(7, f.getRua());
                pst.setString(8,f.getTelefone());
                pst.setInt(9, f.getIdFunc());
                
                pst.execute();
           } catch (SQLException error) {
                JOptionPane.showMessageDialog(null, error);
           }
     }
}

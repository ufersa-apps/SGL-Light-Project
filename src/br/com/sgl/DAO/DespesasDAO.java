/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sgl.DAO;
import br.com.sgl.databse.ConnectionFactory;
import br.com.sgl.projeto.Despesa;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Jadson Bruno
 */

public class DespesasDAO {
    Connection con;
    
    public DespesasDAO(){
        try { 
            this.con = ConnectionFactory.getConection("jdbc:postgresql://localhost:5432/sgl", "postgres", "brasfoot");
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null,"Erro na conexÃ£o com o Banco de Dados");
        }
    }
    
    public void cadastraDespesa(Despesa d){
        String sql = "insert into despesas (descricao,valor,data) values (?,?,?)";
        
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1,d.getDescricao());
            pst.setFloat(2,d.getValor());
            pst.setDate(3, Date.valueOf(d.getData()));
            pst.execute();
        } catch (SQLException error) {
             JOptionPane.showMessageDialog(null, error);
        }
    }
    /**Retorna um objeto Despesa ou null, caso o ID passado não seja encontrado. */
    public Despesa buscaDespesa(int id){
        Despesa d = null;
        String sql = "select * from despesas where id_desp = ?"; 
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                d = new Despesa(rs.getString("descricao"),rs.getFloat("valor"),rs.getDate("data").toLocalDate());
                d.setId(rs.getInt("id_desp"));
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        return d;
    }
    /**Retorna um List<Despesa> contendo todas despesas do Banco de Dados, ou vazio, caso não exista nehuma despesa. */
    public List<Despesa> ListaDespesas(){
        List<Despesa> lsd = new ArrayList<>();
        String sql = "select*from despesas";
            try {
               PreparedStatement pst = con.prepareStatement(sql);
               ResultSet rs = pst.executeQuery();
               while(rs.next()){
                  Despesa d = new Despesa(rs.getString("descricao"),rs.getFloat("valor"),rs.getDate("data").toLocalDate());
                  d.setId(rs.getInt("id_desp"));
                  lsd.add(d);
               }
           } catch (SQLException error) {
               JOptionPane.showMessageDialog(null, error);
           }
        return lsd;
    }
    
    public void editaDespesa(Despesa d){
        String sql = "update despesas set descricao = ?,valor = ?,data = ? where id_desp = ?";
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1,d.getDescricao());
            pst.setFloat(2, d.getValor());
            pst.setDate(3,Date.valueOf(d.getData()));
            pst.setInt(4, d.getId());
            pst.execute();
        } catch (SQLException e) {
        }
    }
}

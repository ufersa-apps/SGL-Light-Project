package br.com.sgl.DAO;

import br.com.sgl.databse.ConnectionFactory;
import java.sql.*;
import javax.swing.JOptionPane;
import br.com.sgl.projeto.ControleEstoque;
import br.com.sgl.projeto.Produto;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ControleEstoqueDAO {
    private Connection con;
       public ControleEstoqueDAO(){
        try { 
            this.con = ConnectionFactory.getConection("jdbc:postgresql://localhost:5432/sgl", "postgres", "brasfoot");
        } catch (ClassNotFoundException ex) {
         JOptionPane.showMessageDialog(null,"Erro na conexão com o Banco de Dados");
        }
       }
       
       public void cadastraMercadoria(Produto mercadoria, int quantidade) throws SQLException{
           String sql = "insert into produto (descricao, preco, quantidade, preco_compra,validade) values (?,?,?,?,?)";
           
           try {
                PreparedStatement pst = con.prepareStatement(sql);
                
                pst.setString(1,mercadoria.getNome());
                pst.setFloat(2,mercadoria.getValor());
                pst.setInt(3,quantidade);
                pst.setFloat(4,mercadoria.getValorCompra());
                pst.setDate(5, Date.valueOf(mercadoria.getValidade()));
                pst.execute();
           } catch (SQLException error) {
                JOptionPane.showMessageDialog(null, error);
           }
           
       }
       /**Modifica os atributos no Banco de Dados de acordo com o Produto passado como parÃ¢metro.*/
       public void editarInfoProduto(Produto mercadoria,int id_produto) throws SQLException{
           String sql = "update produto set descricao = ?,preco =?, preco_compra = ?, validade = ? where id_pro = ?";
           try {
               PreparedStatement pst = con.prepareStatement(sql);  
               pst.setString(1, mercadoria.getNome());
               pst.setFloat(2,mercadoria.getValor());
               pst.setFloat(3,mercadoria.getValorCompra());
               pst.setDate(4, Date.valueOf(mercadoria.getValidade()));
               pst.setInt(5, id_produto);
               
               pst.execute();
           } catch (SQLException error) {
               JOptionPane.showMessageDialog(null, error);
           }
       }
       
       /**Modifica a quantidade de um produto especÃ­fico no Banco de Dados.*/
       public void editaQuantProduto(Produto mercadoria, int novaQuantidade){
           String sql = "update produto set quantidade = ? where id_pro = ?";
           try {
               PreparedStatement pst = con.prepareStatement(sql);  
               pst.setInt(1, novaQuantidade);
               pst.setInt(2, mercadoria.getId());
               
               pst.execute();

           } catch (SQLException error) {
               JOptionPane.showMessageDialog(null, error);
           }
       }
       
       /**Lista todos os produtos armazenados no Banco de Dados */
       public List<Produto> listaProduto() throws SQLException{
           String sql = "select*from produto";
           
           List<Produto> lpro =  new ArrayList<>();
           try {
               PreparedStatement pst = con.prepareStatement(sql);
               ResultSet rs = pst.executeQuery();
               while(rs.next()){
                  Produto p = new Produto(rs.getString("descricao")," ",rs.getFloat("preco"),rs.getFloat("preco_compra"),rs.getInt("quantidade"));
                  p.setId(rs.getInt("id_pro"));
                  p.setValidade(rs.getDate("validade").toLocalDate());
                  lpro.add(p);
               }
               
           } catch (SQLException error) {
               JOptionPane.showMessageDialog(null, error);
           }
            return lpro;
       }
       /**Retorna um Produto com o ID passado como parÃ¢metro ou null caso nÃ£o encontre.*/
       public Produto buscaProduto(int id){
           String sql = "select *from produto where id_pro = ?";
           Produto p = null;
           try {
               PreparedStatement pst = con.prepareStatement(sql);
               pst.setInt(1, id);
               ResultSet rs = pst.executeQuery();
               while(rs.next()){
                    p = new Produto(rs.getString("descricao")," ",rs.getFloat("preco"),rs.getFloat("preco_compra"),rs.getInt("quantidade"));
                    p.setId(rs.getInt("id_pro"));
                    p.setValidade(rs.getDate("validade").toLocalDate());
               }
                
           } catch (SQLException e) {
               JOptionPane.showMessageDialog(null, e);
           }
           
           return p;
       }
       /**Retorna um Produto com a descriÃ§Ã£o passada como parÃ¢metro ou null caso nÃ£o encontre.*/ 
       public Produto buscaProduto(String descricao){
           String sql = "select *from produto where descricao like ?";
           Produto p = null;
           try {
               PreparedStatement pst = con.prepareStatement(sql);
               String d = "%"+descricao;
               pst.setString(1, d);
               ResultSet rs = pst.executeQuery();
               while(rs.next()){
                    p = new Produto(rs.getString("descricao")," ",rs.getFloat("preco"),rs.getFloat("preco_compra"),rs.getInt("quantidade"));
                    p.setId(rs.getInt("id_pro"));
                    p.setValidade(rs.getDate("validade").toLocalDate());
               }
                
           } catch (SQLException e) {
               JOptionPane.showMessageDialog(null, e);
           }
           
           return p;
     
       }
       /**Remove o Produto do Banco de dados caso a quantidade de itens desse seja igual a passada como parÃ¢metro ou subtrai a quantidade de itens do produto, caso a quantidade passada como parÃ¢metro seja menor. */
       public void removeProduto(Produto j, int quantidade_remover){
           if(j.getQuantidade() > quantidade_remover){
               this.editaQuantProduto(j, j.getQuantidade() - quantidade_remover);
           }else if (j.getQuantidade() == quantidade_remover){
               String sql1 = "delete from produto where id_pro = ?";
               try {
                   PreparedStatement pst = con.prepareStatement(sql1);
                   pst.setInt(1, j.getId());
                   pst.execute();
                   
               } catch (SQLException e) {
                   JOptionPane.showMessageDialog(null, e);
               } 
           }
       }
       
    /**Encerra a conexÃ£o com o banco de dados. */
       public void closeConnection() throws SQLException{
           con.close();
       }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sgl.DAO;

import br.com.sgl.databse.ConnectionFactory;
import br.com.sgl.projeto.Cliente;
import br.com.sgl.projeto.Conta;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Gabriel Lopes
 */
public class ClienteDAO {
    
    private Connection con;
  
    
    public ClienteDAO() {
        try {
            this.con =  ConnectionFactory.getConection("jdbc:postgresql://localhost:5432/sgl", "postgres", "brasfoot");
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null,"Erro na conexão com o Banco de Dados");
        }
    }
    
    public void cadastraCliente(Cliente c){
        String sql = "insert into cliente (cpf, cidade, bairro, numero, rua, nome, email, telefone, totalcomprado,id_deps) values (?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, c.getCpf());
            pst.setString(2, c.getCidade());
            pst.setString(3, c.getBairro());
            pst.setInt(4, c.getNumero());
            pst.setString(5, c.getRua());
            pst.setString(6, c.getNome());
            pst.setString(7, c.getEmail());
            pst.setString(8, c.getTelefone());
            pst.setFloat(9, c.getTotalComprado());
            pst.setInt(10, c.getIdConta());
          
            pst.execute();
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public void editaCliente(Cliente c){
        String sql = "update cliente set cpf = ?,cidade = ?,bairro = ?,numero = ?,rua = ?,nome = ?,email = ?,telefone = ? where id_cli = ?";
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, c.getCpf());
            pst.setString(2, c.getCidade());
            pst.setString(3, c.getBairro());
            pst.setInt(4, c.getNumero());
            pst.setString(5, c.getRua());
            pst.setString(6, c.getNome());
            pst.setString(7, c.getEmail());
            pst.setString(8, c.getTelefone());
            pst.setInt(9, c.getIdCliente());
            pst.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    public void cadastraConta(Cliente c){
        String sql = "insert into debitos (valor) values (?)";
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            Conta con = c.getContaCliente();
            pst.setFloat(1, con.getCrediario());
            pst.execute();
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    
    public void cadastraConta(Conta c, int id_cliente){
        String sql = "insert into debitos (data, valor) values (?,?)";
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            if(c.getData() != null){
                pst.setDate(1, Date.valueOf(c.getData()));
            }else{
                pst.setDate(1, Date.valueOf(LocalDate.MIN));
            }
            pst.setFloat(2, c.getCrediario());
            
            pst.execute();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    public void editaConta(Conta c){
        String sql = "update debitos set  data = ?,valor = ? where id_deps = ?";
         try {             
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setDate(1, Date.valueOf(c.getData()));
            pst.setFloat(2, c.getCrediario());
            pst.setInt(3, c.getId_deps());
            pst.execute();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,e);
        }
    }
    
    public Conta buscaConta(int id){
        Conta c = null;
        String sql = "select *from debitos where id_deps = ?";
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                c = new Conta(rs.getFloat("valor"));
                c.setId_deps(rs.getInt("id_deps"));
                if(rs.getDate("data") != null){
                    c.setData(rs.getDate("data").toLocalDate());
                }else{
                    c.setData(LocalDate.MIN);
                }
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return  c;        
    }  
    /**Método recebe o ID do cliente como String e retorna a conta correspondente ou null, caso não seja encontrado.*/
    public Conta buscaConta(String idCliente){
        Conta c = null;
        String sql = "select *from debitos where cli_id = ?";
        int id = Integer.parseInt(idCliente);
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                c = new Conta(rs.getFloat("valor"));
                c.setId_deps(rs.getInt("id_deps"));
                c.setData(rs.getDate("data").toLocalDate());
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return c;
    }
    
     public Cliente buscaCliente(String nome){
        String sql = "select * from cliente where nome like ?";
        Cliente c = null;
        Conta cot;

        try {
            PreparedStatement bd = con.prepareStatement(sql);
            String buscar = nome+"%";
            bd.setString(1, buscar);
            ResultSet rs = bd.executeQuery();
            while(rs.next()){
                c = new Cliente(null,rs.getString("telefone"),rs.getString("email"),rs.getString("nome"), rs.getString("rua"), rs.getInt("numero"), rs.getString("bairro"), rs.getString("cidade"), rs.getString("cpf"));
                c.setTotalComprado(rs.getFloat("totalComprado"));
                c.setIdCliente(rs.getInt("id_cli"));
                c.setIdConta(rs.getInt("id_deps"));
            }
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
      
        return c;        
    }
     
    private Cliente buscaClienteSegundoNome(String nome){
            String sql2 = "select * from cliente where nome like ?";
            Cliente c = null;
            
                try {
                   PreparedStatement bd2 = con.prepareStatement(sql2);
                   String buscar2 = "%"+nome;
                   bd2.setString(1, buscar2);
                   ResultSet rs2 = bd2.executeQuery();
                   while(rs2.next()){
                       c = new Cliente(null,rs2.getString("telefone"),rs2.getString("email"),rs2.getString("nome"), rs2.getString("rua"), rs2.getInt("numero"), rs2.getString("bairro"), rs2.getString("cidade"), rs2.getString("cpf"));
                       c.setTotalComprado(rs2.getFloat("totalComprado"));
                       c.setIdCliente(rs2.getInt("id_cli"));
                       c.setIdConta(rs2.getInt("id_deps"));
                   }
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }    
           return c;     
    } 
    public Cliente buscaClienteID(int id){
        Cliente c = null;
        String sql = "select *from cliente where id_cli = ?";       
        try {
               PreparedStatement pst = con.prepareStatement(sql);
               pst.setInt(1, id);
               ResultSet rs = pst.executeQuery();
               
               
               while(rs.next()){
                    c = new Cliente(null ,rs.getString("telefone"),rs.getString("email"),rs.getString("nome"), rs.getString("rua"), rs.getInt("numero"), rs.getString("bairro"), rs.getString("cidade"), rs.getString("cpf"));
                    c.setTotalComprado(rs.getFloat("totalComprado"));
                    c.setIdCliente(rs.getInt("id_cli"));
                    c.setIdConta(rs.getInt("id_deps"));
               }
               if(c != null)
                c.setContaCliente(buscaConta(c.getIdConta()));
                
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }

        return c;
    }
    
    public List<Cliente> listaCliente(){
        String bd = "select*from cliente";
        List<Cliente> listCli = new ArrayList<Cliente>();
        try {
            PreparedStatement sql = con.prepareStatement(bd);
            ResultSet rs = sql.executeQuery();
            while(rs.next()){

                Cliente c = new Cliente(null,rs.getString("telefone"),rs.getString("email"),rs.getString("nome"), rs.getString("rua"), rs.getInt("numero"), rs.getString("bairro"), rs.getString("cidade") , rs.getString("cpf"));
                c.setAniversario(rs.getDate("nascimento").toLocalDate());
                c.setTotalComprado(rs.getFloat("totalComprado"));
                c.setIdCliente(rs.getInt("id_cli"));
                c.setIdConta(rs.getInt("id_deps"));
                listCli.add(c);                
            }            
        } catch (SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }
        return  listCli;    
    }
    
    public void removeCliente(Cliente c){        
        String s = "delete from cliente where id_cli = ?";
        try {
            PreparedStatement pst = con.prepareStatement(s);
            pst.setInt(1, c.getIdCliente());
            removeConta(c.getIdCliente());
            pst.execute();
        } catch (SQLException e){
            JOptionPane.showMessageDialog(null, "Erro ao remover cliente do Banco de dados");
        }          
    }
    
    public void removeConta(int c){
        String s = "delete from debitos where id_deps = ?";
        try {
            PreparedStatement pst = con.prepareStatement(s);
            pst.setInt(1, c);
            pst.execute();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    public int idUltimoCliente(){
        int id = 0;
        String sql = "select max(id_cli) as id from cliente";
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                id = rs.getInt("id");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }            
        return id;
    }
    
    public int idUltimaConta(){
        int id = 0;
        String sql = "select max(id_deps) as id from debitos";
        
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                id = rs.getInt("id");
            }
        } catch (SQLException ex) {
          JOptionPane.showMessageDialog(null, ex);
        }
        
        return id;
    }
    
}

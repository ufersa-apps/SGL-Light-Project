/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sgl.DAO;

import br.com.sgl.databse.ConnectionFactory;
import br.com.sgl.projeto.Compra;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Calendar;
import javax.swing.JOptionPane;
import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jadson Bruno
 */
public class VendaDAO {
    private Connection con;
    LocalDate ldS = null;
    LocalTime ltS = null;
    public VendaDAO() {
       try { 
            this.con = ConnectionFactory.getConection("jdbc:postgresql://localhost:5432/sgl", "postgres", "brasfoot");
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null,"Erro na conexÃ£o com o Banco de Dados");
        }
    }

    public void cadastraVenda(Compra c,int id_func,int id_cliente){
         String sql = "insert into participa_venda (func_id,cliente_id,data,hora,valor) values (?,?,?,?,?)";
          try {
              
                PreparedStatement pst = con.prepareStatement(sql);
                pst.setInt(1, id_func);
                pst.setInt(2, id_cliente);
                LocalDate ld = LocalDate.now();
                Date d = Date.valueOf(ld);
                Time hora = Time.valueOf(LocalTime.now());
                pst.setDate(3, d);
                pst.setTime(4, hora);
                pst.setFloat(5, c.getSubTotal());

                pst.execute();
                this.ldS = ld;
                this.ltS = LocalTime.now();
           } catch (SQLException error) {
                JOptionPane.showMessageDialog(null, error);
           }
          
    }
    public void removeVenda(int id_cliente){
         String sql = "delete from participa_venda where cliente_id = ?";
         PreparedStatement pst;
        try {
            pst = con.prepareStatement(sql);
            pst.setInt(1, id_cliente);
            pst.execute();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    public Compra buscaCompra(int id){
         String sql = "select * from participa_venda where venda_id = ?";
         Compra c = null;
           try {
               PreparedStatement pst = con.prepareStatement(sql);
               pst.setInt(1, id);
               ResultSet rs =  pst.executeQuery();
               while(rs.next()){
                   c = new Compra(rs.getDate("data").toLocalDate(), rs.getTime("hora").toLocalTime(), rs.getFloat("valor"));
                   c.setId_venda(rs.getInt("venda_id"));
               }
                pst.execute();
           } catch (SQLException error) {
                JOptionPane.showMessageDialog(null, error);
           }
           return c;
    }
    
    public List<Compra> listaCompras(){
        String sql = "select * from participa_venda";
        List<Compra> lc = new ArrayList<>();
           try {
               PreparedStatement pst = con.prepareStatement(sql);
               ResultSet rs = pst.executeQuery();
               while(rs.next()){
                  Compra f = new Compra(rs.getDate("data").toLocalDate(), rs.getTime("hora").toLocalTime(), rs.getFloat("valor"));
                  f.setId_venda(rs.getInt("venda_id"));
                  lc.add(f);
               }
           } catch (SQLException error) {
               JOptionPane.showMessageDialog(null, error);
           }
        return lc;
    }
    /**Retorna o ID da última compra realizada, ou 0 caso não haja registros.*/
    public int idUltimaCompra(){
        int id = 0;
        String sql = "select max(venda_id) as id from participa_venda ";
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                id = rs.getInt("id");
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        return id;
    }
    public LocalDate getLdS() {
        return ldS;
    }

    public void setLdS(LocalDate ldS) {
        this.ldS = ldS;
    }

    public LocalTime getLtS() {
        return ltS;
    }

    public void setLtS(LocalTime ltS) {
        this.ltS = ltS;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sgl.DAO;

import br.com.sgl.databse.ConnectionFactory;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import br.com.sgl.projeto.BonificaFuncionario;
import javax.swing.JOptionPane;

/**
 *
 * @author Jadson Bruno
 */
public class BonificaFuncionarioDAO {
       Connection con;

    public BonificaFuncionarioDAO() {
           try {
               this.con = ConnectionFactory.getConection("jdbc:postgresql://localhost:5432/sgl", "postgres", "brasfoot");
           } catch (ClassNotFoundException ex) {
               Logger.getLogger(BonificaFuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
           }
    }
    
    public void cadastraBonificacaoFunc(BonificaFuncionario b){
        String sql = "insert into bonificacaofuncionario (bonus,meta,estado) values (?,?,?)";
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setFloat(1, b.getBonus());
            pst.setFloat(2, b.getMetas());
            pst.setBoolean(3, b.getAtivo());
            pst.execute();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,e);
        } 
    }
    
    public BonificaFuncionario buscaBonificacaoFunc(){
        String sql = "select * from bonificacaofuncionario";
        BonificaFuncionario bf = new BonificaFuncionario();
         
           try {
                PreparedStatement   pst = con.prepareStatement(sql);      
                ResultSet rs = pst.executeQuery();
                while(rs.next()){
                    bf.setMeta(rs.getFloat("meta"));
                    bf.setBonus(rs.getFloat("bonus"));
                    bf.setAtivo(rs.getBoolean("estado"));
                }
           } catch (SQLException ex) {
              JOptionPane.showMessageDialog(null, ex);
           }
           return bf;
    }
       
    public void editaBonificacaoFunc(BonificaFuncionario bf){
        String sql = "update bonificacaofuncionario set  meta = ?,bonus = ?,estado = ?";
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setFloat(1, bf.getBonus());
            pst.setFloat(2, bf.getMetas());
            pst.setBoolean(3, bf.getAtivo());
            pst.execute();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,e);
        }
    }
    
}

package br.com.sgl.DAO;
import br.com.sgl.databse.ConnectionFactory;
import br.com.sgl.projeto.Login;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author Jadson Bruno
 */
public class LoginDAO {
    private Connection con;
    
    public LoginDAO(){
        try {
            this.con =ConnectionFactory.getConection("jdbc:postgresql://localhost:5432/sgl", "postgres", "brasfoot");
        } catch (ClassNotFoundException ex) {
           JOptionPane.showMessageDialog(null,"Erro na conexão com o Banco de Dados");
        }
    }

    public void editaLogin(String n, String s) throws SQLException{
        Login l = new Login();
        l.setNome(n);
        l.setSenha(s);
            String sql = "update admin set login = ? ,senha = ?";
            try {
                PreparedStatement pst = con.prepareStatement(sql);
                pst.setString(1,n);
                pst.setString(2,s);
                    pst.execute();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }
    }
    
    /**Retorna true caso o Login seja igual ao armazenado no Banco de Dados ou false caso contrário.*/ 
    public boolean validaLogin(Login l ){
        boolean result = false;
        String sql = "select*from admin where login = ? and senha = ?";
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1,l.getNome());
            pst.setString(2,l.getSenha());
            ResultSet rs = pst.executeQuery();
            
            Login ln = null;
            while(rs.next()){
                ln = new Login();
                ln.setNome(rs.getString("login"));
                ln.setSenha(rs.getString("senha"));
            }
            if(ln != null){
                result = ln.getNome().equals(l.getNome()) && ln.getSenha().equals(l.getSenha());
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        return result;
    }
    public String login_anterior(){
        String sql = "select (login) from admin";
        String login = "";
        PreparedStatement pst;
        try {
            pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                login = rs.getString("login");
            }
        } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, ex);
        }
        
        return login;
    }
    /**Encerra a conexão com o banco de dados. */
     public void closeConnection() throws SQLException{
           con.close();
       }
}

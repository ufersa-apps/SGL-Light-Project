/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sgl.DAO;
import br.com.sgl.databse.ConnectionFactory;
import br.com.sgl.projeto.Fornecedor;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


/**
 *
 * @author Jadson Bruno
 */
public class FornecedorDAO {
    Connection con;

    public FornecedorDAO() {
        try {
            this.con =  ConnectionFactory.getConection("jdbc:postgresql://localhost:5432/sgl", "postgres", "brasfoot");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FornecedorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void cadastraFornecedor(Fornecedor f){
        String sql = "insert into fornecedor (nome,telefone,email,cnpj) values (?,?,?,?)";
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            
            pst.setString(1,f.getNome());
            pst.setString(2,f.getTelefone());
            pst.setString(3, f.getEmail());
            pst.setString(4,f.getCnpj());
            
            pst.execute();
        } catch (SQLException error) {
             JOptionPane.showMessageDialog(null, error);
        }
    }
    /**Retorna um Fornecedor caso seja encontrado, ou null, caso não exista um Fornecedor com o mesmo ID no Banco de Dados.*/
    public Fornecedor buscaFornecedor(int id){
        Fornecedor d = null;
        String sql = "select * from fornecedor where id_forn = ?"; 
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                d = new Fornecedor(rs.getString("nome"),rs.getString("telefone"),rs.getString("email"),rs.getString("cnpj"));
                d.setIdFor(rs.getInt("id_forn"));
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        return d;
    }
    /**Retorna um List<Fornecedor> contendo os Fornecedores salvos no Banco de Dados, ou um List<Fornecedor> vazio, caso não haja Fornecedores salvos.*/
    public List<Fornecedor> listaFornecedores(){
        List<Fornecedor> lsd = new ArrayList<>();
        String sql = "select*from fornecedor";
            try {
               PreparedStatement pst = con.prepareStatement(sql);
               ResultSet rs = pst.executeQuery();
               while(rs.next()){
                  Fornecedor d = new  Fornecedor(rs.getString("nome"),rs.getString("telefone"),rs.getString("email"),rs.getString("cnpj"));
                  d.setIdFor(rs.getInt("id_forn"));
                  lsd.add(d);
               }
           } catch (SQLException error) {
               JOptionPane.showMessageDialog(null, error);
           }
        return lsd;
    }
    
    public void editarFornecedor(Fornecedor f) throws SQLException{
           String sql = "update fornecedor set nome = ?,email =?, telefone = ?, cnpj = ? where id_forn = ?";
           try {
               PreparedStatement pst = con.prepareStatement(sql);  
               pst.setString(1, f.getNome());
               pst.setString(2,f.getEmail());
               pst.setString(3,f.getTelefone());
               pst.setString(4, f.getCnpj());
               pst.setInt(5,f.getIdFor());
               
               pst.execute();
           } catch (SQLException error) {
               JOptionPane.showMessageDialog(null, error);
           }
       }
    
    
    
}

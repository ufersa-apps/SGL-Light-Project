/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sgl.DAO;

import br.com.sgl.databse.ConnectionFactory;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import br.com.sgl.projeto.BonificacaoCliente;
import javax.swing.JOptionPane;

/**
 *
 * @author belarsn
 */

public class BonificaClienteDAO {
    Connection con;
    
    public BonificaClienteDAO(){
        try {
            this.con = ConnectionFactory.getConection("jdbc:postgresql://localhost:5432/sgl", "postgres", "brasfoot");
        } catch (ClassNotFoundException ex){
            Logger.getLogger(BonificaClienteDAO.class.getName()).log(Level.SEVERE,null,ex);
        }
    }
    
    public void cadastraBonificaFunc(BonificacaoCliente bc){
        String sql = "insert into bonificacaocliente (valorLimite,desconto,estado) values (?,?,?)";
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setFloat(1, bc.getlimite());
            pst.setFloat(2, bc.getDesconto());
            pst.setBoolean(3, bc.getAtivo());
            pst.execute();
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }
    } 
    
    public BonificacaoCliente buscaBonificaClientFunc(){
        String sql = "select * from bonificacaocliente";
        BonificacaoCliente b_c = new BonificacaoCliente();
        
        try{
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
             while(rs.next()){
                    b_c.setLimite(rs.getFloat("valorLimite"));
                    b_c.setDesconto_(rs.getFloat("desconto"));
                    b_c.bonusAtivo(rs.getBoolean("estado"));
                }
        } catch (SQLException e_){
            JOptionPane.showMessageDialog(null, e_);
        }
        return b_c;
    }
    
    public void editaBonificaClientFunc(BonificacaoCliente b_1){
        String sql = "update bonificacaocliente set  valorLimite = ?,desconto = ?,estado = ?";
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setFloat(1, b_1.getlimite());
            pst.setFloat(2, b_1.getDesconto());
            pst.setBoolean(3, b_1.getAtivo());
            pst.execute();
        } catch (SQLException e1) {
            JOptionPane.showMessageDialog(null,e1);
        }
    
    }
}